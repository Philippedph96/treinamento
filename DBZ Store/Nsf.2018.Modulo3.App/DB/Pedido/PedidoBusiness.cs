﻿using Nsf._2018.Modulo3.App.DB.PedidoItem;
using Nsf._2018.Modulo3.App.DB.Produto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    class PedidoBusiness
    {
        public int Salvar (PedidoDTO pedido, List<ProdutoDTO> produto)
        {
            PedidoDatabase db = new PedidoDatabase();
            int idpedido = db.Salvar(pedido);

            PedidoItemBusiness business = new PedidoItemBusiness();
            foreach (ProdutoDTO item in produto)
            {
                PedidoItemDTO dto = new PedidoItemDTO();
                dto.IDPedido = idpedido;
                dto.IDProduto = item.ID;

                business.Salvar(dto);
            }
            return idpedido;
        }

    }
}
