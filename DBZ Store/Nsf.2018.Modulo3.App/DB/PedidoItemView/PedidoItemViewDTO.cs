﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.PedidoItemView
{
    class PedidoItemViewDTO
    {
        public int IDPedido { get; set; }

        public string Cliente { get; set; }

        public DateTime Data { get; set; }

        public int IDPedidoItem { get; set; }

        public Decimal Preco { get; set; }
    }
}
