﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    class ProdutoBusiness
    {
        ProdutoDatabase db = new ProdutoDatabase();

        public int Salvar(ProdutoDTO dto)
        {
            if (dto.Produto == string.Empty)
            {
                throw new ArgumentException("O PRODUTO precisa ser preenchido");
            }
            if (dto.Valor == 0) 
            {
                throw new ArgumentException("O VALOR precisa ser preenchido");
            }

            return db.Salvar(dto);

        }
        public void Alterar (ProdutoDTO dto)
        {
            if (dto.Produto == string.Empty)
            {
                throw new ArgumentException("O PRODUTO precisa ser preenchido");
            }
            if (dto.Valor == 0)
            {
                throw new ArgumentException("O VALOR precisa ser preenchido");
            }

            db.Alterar(dto);
        }
        public void Remover (ProdutoDTO dto)
        {
            db.Remover(dto);
        }
        public List<ProdutoDTO> Consultar(ProdutoDTO dto)
        {
            List<ProdutoDTO> consultar = db.Consultar(dto);
            return consultar;
        }
        public List<ProdutoDTO> Listar()
        {
            List<ProdutoDTO> consultar = db.Listar();
            return consultar;
        }
    }      
}  
