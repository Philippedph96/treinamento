﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.DB.PedidoItemView;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmPedidoConsultar : UserControl
    {
        public frmPedidoConsultar()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PedidoItemViewDTO dto = new PedidoItemViewDTO();
            dto.Cliente = txtCliente.Text;

            PedidoItemViewDatabase db = new PedidoItemViewDatabase();
            List<PedidoItemViewDTO> list = db.Consultar(dto);

            dgvPedidos.AutoGenerateColumns = false;
            dgvPedidos.DataSource = list;
            
            //this.Hide();
        }

        
    }
}
