﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.DB.Pedido;
using Nsf._2018.Modulo3.App.DB.Produto;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmPedidoCadastrar : UserControl
    {
        BindingList<ProdutoDTO> carrinho = new BindingList<ProdutoDTO>();

        public frmPedidoCadastrar()
        {
            InitializeComponent();
            CarregarCombo();
            ConfigurarGrid();
        }
        public void CarregarCombo ()
        {
            ProdutoBusiness db = new ProdutoBusiness();
            List<ProdutoDTO> lista = db.Listar();

            cboProduto.ValueMember = nameof(ProdutoDTO.ID);
            cboProduto.DisplayMember = nameof(ProdutoDTO.Produto);
            cboProduto.DataSource = lista;
        }
        public void ConfigurarGrid()
        {
            dgvItens.AutoGenerateColumns = false;
            dgvItens.DataSource = carrinho;
        }

        private void btnEmitir_Click(object sender, EventArgs e)
        {
            PedidoDTO dto = new PedidoDTO();
            dto.Cliente = txtCliente.Text;
            dto.CPF = int.Parse(txtCpf.Text);
            dto.Data = DateTime.Now;

            PedidoBusiness db = new PedidoBusiness();
            db.Salvar(dto, carrinho.ToList());
            MessageBox.Show("Pedido salvo com sucesso",
                            "Sucesso!!",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
            this.Hide();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            ProdutoDTO dto = cboProduto.SelectedItem as ProdutoDTO;
            for (int i = 0; i < int.Parse(txtQuantidade.Text); i++)
            {
                carrinho.Add(dto);
            }
        }
    }
}
