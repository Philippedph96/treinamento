﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.DB.Produto;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmProdutoCadastrar : UserControl
    {
        public frmProdutoCadastrar()
        {
            InitializeComponent();
        }

        private void btnSalvar_Click(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {
                    ProdutoDTO dto = new ProdutoDTO();
                    dto.Produto = txtProduto.Text;
                    dto.Valor = Convert.ToDecimal(txtPreco.Text);

                    ProdutoBusiness busi = new ProdutoBusiness();
                    busi.Salvar(dto);

                    MessageBox.Show("Produto cadastrado com sucesso",
                                    "Sucesso!!",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                }

            }
            catch (ArgumentException f)
            {
                EnviarMensagemErro(f.Message);                
            }
            
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.Produto = txtProduto.Text;
                dto.Valor = Convert.ToDecimal(txtPreco.Text);

                ProdutoBusiness busi = new ProdutoBusiness();
                busi.Salvar(dto);

                MessageBox.Show("Produto cadastrado com sucesso",
                                "Sucesso!!",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
            catch (ArgumentException ex)
            {
                EnviarMensagemErro(ex.Message);
            }
            catch (Exception pk)
            {
                EnviarMensagemErro("Esta ocorrendo um erro na inserção das informações. " +
                    pk.Message);
            }

        }
        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "ERRO!",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
    }
}
